using common.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using pelanggan.Services.RecipeService;
using Redis.Configuration;
using Redis;
using common.Helper.HashGenerator;
using common.Helper.TokenGenerator;
using pelanggan.Services.OrderService;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

ConfigurationManager configuration = builder.Configuration;

builder.Services.AddDbContext<DemoDbContext>(_ =>
{
    _.UseMySql("server=localhost;port=3306;user=root;database=demo", Microsoft.EntityFrameworkCore.ServerVersion.Parse("10.4.25-mariadb"));
});

//redis
var redisOptions = configuration.GetSection("RedisServer").Get<RedisOption>();
if (redisOptions.Enabled)
    builder.Services.UseRedis(redisOptions.RedisCacheConfiguration);

builder.Services.AddScoped<IRecipeService, RecipeService>();
builder.Services.AddScoped<IOrderService, OrderService>();
builder.Services.AddScoped<ITokenGenerator, TokenGenerator>();
builder.Services.AddScoped<IHashGenerator, HashGenerator>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
