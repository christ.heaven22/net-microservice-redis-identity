﻿using common.Data.RecipeData;
using common.Data.Response;
using common.Helper.HashGenerator;
using common.Helper;
using common.Model;
using Newtonsoft.Json;
using Redis.Constants;
using Redis.Service;
using common.Data.OrderData;
using Microsoft.EntityFrameworkCore;

namespace pelanggan.Services.OrderService
{
    public class OrderService : IOrderService
    {
        private readonly DemoDbContext _DBContext;
        private readonly IRedisService _redisService;
        private readonly IHashGenerator _hashGenerator;
        public OrderService(DemoDbContext DBContext, IRedisService redisService, IHashGenerator hashGenerator)
        {
            _DBContext = DBContext;
            _redisService = redisService;
            _hashGenerator = hashGenerator;
        }

        public async Task<List<OrderData>> GetAll()
        {
            var Data = new List<OrderData>();

            Data = await _DBContext.Activeorders.Select(
             s => new OrderData(s)
           ).ToListAsync();

            if (Data.Count < 0) return new List<OrderData>();
            else
            {
                return Data;
            }

        }
        public async Task<List<OrderViewData>> GetById(int TableId)
        {
            var all = await GetAll();
            var result = all.FindAll(t => t.IdGuestTable == TableId);

            if (result == null) throw new ErrorException(ErrorUtil.noTableOrder);

            List<OrderViewData> resulte = new List<OrderViewData>();

            foreach (var detail in result)
            {
                var detailMenu = _DBContext.Recipes.FirstOrDefault(t => t.Id == detail.IdRecipe);
                if (detailMenu != null)
                {
                    resulte.Add(new OrderViewData(detail, detailMenu));
                }  
            }

            return resulte;
        }

        public async Task<List<OrderData>> Add(List<OrderData> data)
        {
            List<OrderData> result = new List<OrderData>();

            foreach (var detail in data)
            {

                var entity = new Activeorder()
                {
                    IdGuestTable = detail.IdGuestTable,
                    IdRecipe = detail.IdRecipe,
                    CreateDate = detail.CreateDate,
                    QtyOrder = detail.QtyOrder,
                };

                _DBContext.Activeorders.Add(entity);
                await _DBContext.SaveChangesAsync();

                result.Add(new OrderData(entity));
            }
            return result;
        }
    }
}