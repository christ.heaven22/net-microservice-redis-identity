﻿using common.Data.OrderData;
using common.Data.RecipeData;

namespace pelanggan.Services.OrderService
{
    public interface IOrderService
    {
        Task<List<OrderData>> GetAll();
        Task<List<OrderViewData>> GetById(int TableId);
        Task<List<OrderData>> Add(List<OrderData> data);
    }
}
