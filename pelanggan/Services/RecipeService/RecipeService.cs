﻿using common.Data.RecipeData;
using common.Data.Response;
using common.Helper.HashGenerator;
using common.Helper;
using common.Model;
using Newtonsoft.Json;
using Redis.Constants;
using Redis.Service;
using Microsoft.EntityFrameworkCore;

namespace pelanggan.Services.RecipeService
{
    public class RecipeService : IRecipeService
    {
        private readonly DemoDbContext _DBContext;
        private readonly IRedisService _redisService;
        private readonly IHashGenerator _hashGenerator;
        public RecipeService(DemoDbContext DBContext, IRedisService redisService, IHashGenerator hashGenerator)
        {
            _DBContext = DBContext;
            _redisService = redisService;
            _hashGenerator = hashGenerator;
        }

        public async Task<List<RecipePelangganData>> GetAll()
        {
            var Data = new List<RecipePelangganData>();

            var UserCacheData = await _redisService.GetCacheString(RedisKeyConstants.RECIPE_PELANGGAN_MODEL);
            if (!string.IsNullOrEmpty(UserCacheData))
            {
                Data = JsonConvert.DeserializeObject<List<RecipePelangganData>>(UserCacheData);
                return Data!;
            }

            Data = await _DBContext.Recipes.Select(
             s => new RecipePelangganData(s)
           ).ToListAsync();

            if (Data.Count < 0) return new List<RecipePelangganData>();
            else
            {
                await _redisService.CacheString(RedisKeyConstants.RECIPE_PELANGGAN_MODEL, JsonConvert.SerializeObject(Data), TimeSpan.FromHours(24));
                return Data;
            }

        }
        public async Task<RecipePelangganData> GetById(int Id)
        {
            var all = await GetAll();
            var result = all.FirstOrDefault(t => t.Id == Id);

            if (result == null) throw new ErrorException(ErrorUtil.NoRecipeFound);

            return result;
        }
    }

}
