﻿using common.Data.RecipeData;

namespace pelanggan.Services.RecipeService
{
    public interface IRecipeService
    {
        Task<List<RecipePelangganData>> GetAll();
        Task<RecipePelangganData> GetById(int Id);
    }

}
