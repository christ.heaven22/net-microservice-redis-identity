﻿using common.Data.RecipeData;
using common.Data.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using pelanggan.Services.RecipeService;

namespace pelanggan.Controllers
{

    [ApiController]
    [Route("api/")]
    public class RecipeController : ControllerBase
    {
        private readonly IRecipeService _RecipeService;

        public RecipeController(
                              IRecipeService recipeService)
        {
            _RecipeService = recipeService;
        }

        [HttpGet("Recipe")]
        [ProducesResponseType(typeof(GenericListResponse<RecipePelangganData>), 400)]
        [ProducesResponseType(typeof(BasicResponse), 200)]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await _RecipeService.GetAll();
                return Ok
                (
                    new GenericListResponse<RecipePelangganData>()
                    {
                        Data = result
                    }
                );
            }
            catch (ErrorException e)
            {
                return BadRequest(new BasicResponse() { Error = e.Error });
            }
        }

        [HttpGet("Recipe/Id/{Id}")]
        [ProducesResponseType(typeof(GenericResponse<RecipePelangganData>), 200)]
        [ProducesResponseType(typeof(Error), 400)]
        public async Task<IActionResult> GetById(int Id)
        {

            try
            {
                var result = await _RecipeService.GetById(Id);
                return Ok(new GenericResponse<RecipePelangganData>() { Data = result });
            }
            catch (ErrorException e)
            {
                return BadRequest(new BasicResponse() { Error = e.Error });
            }
        }

    }

}
