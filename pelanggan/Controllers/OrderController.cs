﻿using common.Data.OrderData;
using common.Data.RecipeData;
using common.Data.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using pelanggan.Services.OrderService;

namespace pelanggan.Controllers
{
    [ApiController]
    [Route("api/")]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _OrderService;

        public OrderController(
                              IOrderService orderService)
        {
            _OrderService = orderService;
        }

        [HttpGet("Order/TableId/{Id}")]
        [ProducesResponseType(typeof(GenericResponse<List<OrderViewData>>), 200)]
        [ProducesResponseType(typeof(Error), 400)]
        public async Task<IActionResult> GetById(int Id)
        {

            try
            {

                var result = await _OrderService.GetById(Id);
                return Ok(new GenericResponse<List<OrderViewData>>() { Data = result });
            }
            catch (ErrorException e)
            {
                return BadRequest(new BasicResponse() { Error = e.Error });
            }
        }

        [HttpPost("Order/add")]
        [ProducesResponseType(typeof(GenericResponse<List<OrderData>>), 200)]
        [ProducesResponseType(typeof(Error), 400)]
        public async Task<IActionResult> InsertUser([FromBody] List<OrderData> data)
        {
            try
            {
                var result = await _OrderService.Add(data);
                return Ok(new GenericResponse<List<OrderData>>() { Data = result });
            }
            catch (ErrorException e)
            {
                return BadRequest(new BasicResponse() { Error = e.Error });
            }
        }

    }
}

