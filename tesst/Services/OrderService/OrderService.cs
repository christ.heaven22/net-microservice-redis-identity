﻿using common.Data.OrderData;
using common.Data.Response;
using common.Helper.HashGenerator;
using common.Helper;
using common.Model;
using Redis.Service;
using Microsoft.EntityFrameworkCore;
using Redis.Constants;

namespace admin.Services.OrderService
{
    public class OrderService : IOrderService
    {
        private readonly DemoDbContext _DBContext;
        private readonly IRedisService _redisService;
        private readonly IHashGenerator _hashGenerator;
        public OrderService(DemoDbContext DBContext, IRedisService redisService, IHashGenerator hashGenerator)
        {
            _DBContext = DBContext;
            _redisService = redisService;
            _hashGenerator = hashGenerator;
        }

        public async Task<List<OrderData>> GetAll()
        {
            var Data = new List<OrderData>();

            Data = await _DBContext.Activeorders.Select(
             s => new OrderData(s)
           ).ToListAsync();

            if (Data.Count < 0) return new List<OrderData>();
            else
            {
                return Data;
            }

        }
        public async Task<List<OrderViewData>> GetById(int TableId)
        {
          
            var all = await GetAll();
            var result = all.FindAll(t => t.IdGuestTable == TableId);

            if (result == null) throw new ErrorException(ErrorUtil.noTableOrder);

            List<OrderViewData> resulte = new List<OrderViewData>();

            foreach (var detail in result)
            {
                var detailMenu = _DBContext.Recipes.FirstOrDefault(t => t.Id == detail.IdRecipe);
                if (detailMenu != null)
                {
                    resulte.Add(new OrderViewData(detail, detailMenu));
                }
            }

           // coba pake rawsql
           var coba = _DBContext.Activeorders.FromSqlRaw($"select a.*, b.name,b.price from demo.activeorder a,recipe b " +
           $"where a.idRecipe = b.Id " +
           $"and a.idGuestTable ={TableId} ").ToList();

            foreach (var detail in coba)
            {
                Console.WriteLine("masuk");
                Console.WriteLine(detail.QtyOrder);
                Console.WriteLine(detail.IdRecipeNavigation.Name);

            }

            return resulte;
        }

        public async Task Delete(int Id)
        {

            var checkDetail = await _DBContext.Activeorders.Where(s => s.IdGuestTable == Id).ToListAsync();
            if (checkDetail == null) throw new ErrorException(ErrorUtil.noTableOrder);

            foreach (var detail in checkDetail)
            {
                _DBContext.Activeorders.Remove(detail);
                await _DBContext.SaveChangesAsync();
            }

        }
    }    
}
