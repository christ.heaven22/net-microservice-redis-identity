﻿using common.Data.OrderData;

namespace admin.Services.OrderService
{
    public interface IOrderService
    {
        Task<List<OrderData>> GetAll();
        Task<List<OrderViewData>> GetById(int TableId);
        Task Delete(int Id);
    }
}
