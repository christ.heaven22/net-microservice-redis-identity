﻿using admin.Services.OrderService;
using admin.Services.UserAuthService;
using common.Data.OrderData;
using common.Data.RecipeData;
using common.Data.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace admin.Controllers
{
    [ApiController]
    [Route("api/")]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _OrderService;
        private readonly IUserAuthService _UserAuthService;
        public OrderController(IUserAuthService userAuthService,
                              IOrderService orderService)
        {
            _OrderService = orderService;
            _UserAuthService = userAuthService;
        }

        [HttpGet("Order/TableId/{Id}")]
        [ProducesResponseType(typeof(GenericResponse<List<OrderViewData>>), 200)]
        [ProducesResponseType(typeof(Error), 400)]
        public async Task<IActionResult> GetById(int Id)
        {

            try
            {
                //var nameClaim = Request.Headers[HeaderNames.Authorization].ToString();
                //var authenticated = _UserAuthService.ValidateToken(nameClaim.Replace("Bearer ", ""));

                //if (!authenticated)
                //    return Unauthorized();

                Console.WriteLine(Id);

                var result = await _OrderService.GetById(Id);
                return Ok(new GenericResponse<List<OrderViewData>>() { Data = result });
            }
            catch (ErrorException e)
            {
                return BadRequest(new BasicResponse() { Error = e.Error });
            }
        }

        [HttpDelete("Order/{tableId}")]
        [ProducesResponseType(typeof(GenericResponse<BasicResponse>), 200)]
        [ProducesResponseType(typeof(Error), 400)]
        public async Task<IActionResult> DeleteUser(int tableId)
        {
            try
            {
                var nameClaim = Request.Headers[HeaderNames.Authorization].ToString();
                var authenticated = _UserAuthService.ValidateToken(nameClaim.Replace("Bearer ", ""));

                if (!authenticated)
                    return Unauthorized();

                await _OrderService.Delete(tableId);
                return Ok(new BasicResponse() { Message = "delete success" });
            }
            catch (ErrorException e)
            {
                return BadRequest(new BasicResponse() { Error = e.Error });
            }
        }
    }
}
