﻿using common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace common.Data.OrderData
{
    public class OrderViewData
    {
        public int IdRecipe { get; set; }
        public int IdGuestTable { get; set; }
        public DateTime CreateDate { get; set; }
        public int QtyOrder { get; set; }
        public string OrderName { get; set; }
        public double Price { get; set; }
        public double SubTotal { get; set; }

        public OrderViewData()
        {

        }
        public OrderViewData(OrderData data, Recipe recipe)
        {
            IdRecipe = data.IdRecipe;
            IdGuestTable = data.IdGuestTable;
            CreateDate = data.CreateDate;
            QtyOrder = data.QtyOrder;
            OrderName = recipe.Name;
            Price = recipe.Price;
            SubTotal = data.QtyOrder * recipe.Price;
        }
    }
}
