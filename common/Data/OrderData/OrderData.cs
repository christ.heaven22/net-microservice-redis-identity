﻿using common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace common.Data.OrderData
{
    public class OrderData
    {
        public int IdRecipe { get; set; }
        public int IdGuestTable { get; set; }
        public DateTime CreateDate { get; set; }
        public int QtyOrder { get; set; }

        public OrderData()
        {

        }

        public OrderData(Activeorder model)
        {
            IdRecipe = model.IdRecipe;
            IdGuestTable = model.IdGuestTable;
            QtyOrder = model.QtyOrder;
            CreateDate = model.CreateDate;
        }
    }
}
