﻿using common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace common.Data.RecipeData
{
    public class RecipePelangganData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }

        public RecipePelangganData()
        {

        }
        public RecipePelangganData(Recipe model)
        {
            Id = model.Id;
            Name = model.Name;
            Price = model.Price;
        }  
    }
}
